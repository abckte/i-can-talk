package pl.sebox.icantalk.ui.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.HashMap;

import android.view.ViewGroup.LayoutParams;
import android.widget.ScrollView;

import pl.sebox.icantalk.MainActivity;
import pl.sebox.icantalk.R;
import pl.sebox.icantalk.databinding.FragmentDashboardBinding;
import pl.sebox.icantalk.service.SpeakService;
import pl.sebox.icantalk.service.database.SentenceDatabase;

public class DashboardFragment extends Fragment {
    public static Integer DATABASE_CODE = 2;
    private FragmentDashboardBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        ScrollView scrollView = binding.getRoot();
        LinearLayout root = binding.linear;

        HashMap<String, Button> buttonsList = new HashMap<>();
        SentenceDatabase.getQuickTalkSentences().forEach((sentence) -> {
            buttonsList.put(sentence.sentence, new Button(MainActivity.thisContext));
        });


        buttonsList.forEach((text, button) -> {
            button.setText(text);
            button.setTextColor(MainActivity.thisContext.getResources().getColor(R.color.black));
            button.setTextSize(22);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("Speaking", ((Button) view).getText().toString());
                    SpeakService.speak(((Button) view).getText().toString());
                }
            });
            root.addView(button);
        });


        return scrollView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}