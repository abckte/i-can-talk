package pl.sebox.icantalk.ui.notifications;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import java.util.HashMap;
import java.util.List;

import pl.sebox.icantalk.MainActivity;
import pl.sebox.icantalk.R;
import pl.sebox.icantalk.databinding.FragmentNotificationsBinding;
import pl.sebox.icantalk.service.SpeakService;
import pl.sebox.icantalk.service.database.AppDatabase;
import pl.sebox.icantalk.service.database.Sentence;
import pl.sebox.icantalk.service.database.SentenceDao;
import pl.sebox.icantalk.service.database.SentenceDatabase;

public class NotificationsFragment extends Fragment {
    public static Integer DATABASE_CODE = 1;
    private NotificationsViewModel notificationsViewModel;
    private FragmentNotificationsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                new ViewModelProvider(this).get(NotificationsViewModel.class);

        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        ScrollView scrollView = binding.getRoot();
        LinearLayout root = binding.linear;

        HashMap<String, Button> buttonsList = new HashMap<>();
        SentenceDatabase.getHighFrequencySentences().forEach((sentence) -> {
            buttonsList.put(sentence.sentence, new Button(MainActivity.thisContext));
        });


        buttonsList.forEach((text, button) -> {
            button.setText(text);
            button.setTextColor(MainActivity.thisContext.getResources().getColor(R.color.black));
            button.setTextSize(22);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("Speaking", ((Button) view).getText().toString());
                    SpeakService.speak(((Button) view).getText().toString());
                }
            });

            root.addView(button);
        });
        return scrollView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}