package pl.sebox.icantalk.service.database;

import android.util.Log;
import android.widget.Button;

import androidx.room.Room;

import java.util.List;

import pl.sebox.icantalk.MainActivity;
import pl.sebox.icantalk.ui.dashboard.DashboardFragment;
import pl.sebox.icantalk.ui.notifications.NotificationsFragment;

public class SentenceDatabase {
    static SentenceDao sentenceDao;
    static List<Sentence> getQuickTalkSentences;
    static List<Sentence> getHighFrequencySentences;

    public static void init() {
        AppDatabase db = Room.databaseBuilder(MainActivity.thisContext,
                AppDatabase.class, "i_can_talk").build();
        sentenceDao = db.sentenceDao();
        new Thread(() -> {
            getQuickTalkSentences = sentenceDao.getQuickTalkSentences();
            getHighFrequencySentences = sentenceDao.getHighFrequencySentences();
            if (getQuickTalkSentences.size() == 0){
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 501,"tak", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 502,"nie", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 503,"ok", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 504,"dlaczego?", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 505,"co tam?", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 506,"nie chcę", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 507,"gdzie?", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 508,"kiedy?", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 509,"dziś", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 510,"jutro", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 511,"myślę że", DashboardFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getQuickTalkSentences.size() + 512,"idę", DashboardFragment.DATABASE_CODE));
                getQuickTalkSentences = sentenceDao.getQuickTalkSentences();
                Log.d("getQuickTalkSentences refreshed", getQuickTalkSentences.toString());
            }
            if (getHighFrequencySentences.size() == 0){
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 701,"Na pewno jest dokładnie, tak jak mówisz.", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 702,"I co jeszcze?", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 703,"hmmm", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 704,"Mam chorę gardło, lekaż zakazał mi mówić.", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 705,"Czarek", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 706,"Paweł", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 707,"Jana", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 708,"co jemy?", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 709,"nie pamiętam", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 710,"za dużo pisania", NotificationsFragment.DATABASE_CODE));
                sentenceDao.insertAll(new Sentence(getHighFrequencySentences.size() + 711,"ja nie przeczę", NotificationsFragment.DATABASE_CODE));
                getHighFrequencySentences = sentenceDao.getHighFrequencySentences();
                Log.d("getHighFrequencySentences refreshed", getHighFrequencySentences.toString());
            }
            Log.d("getQuickTalkSentences", getQuickTalkSentences.toString());
            Log.d("getHighFrequencySentences", getHighFrequencySentences.toString());
        }).start();
    }

    public static List<Sentence> getQuickTalkSentences() {
        return getQuickTalkSentences;
    }

    public static List<Sentence> getHighFrequencySentences() {
        return getHighFrequencySentences;
    }
}
