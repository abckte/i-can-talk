package pl.sebox.icantalk.service;

import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

import pl.sebox.icantalk.MainActivity;

public class SpeakService {
    static TextToSpeech t1 = null;

    public static void checkInit() {
        if (t1 == null) {
            t1 = new TextToSpeech(MainActivity.thisContext, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.ERROR) {
                        Toast.makeText(MainActivity.thisContext, "Error speaking", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            t1.setLanguage(Locale.getDefault());
        }
    }
    public static void init(){
        checkInit();
    }

    public static void speak(String text) {
        checkInit();
        t1.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

}
