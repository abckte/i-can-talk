package pl.sebox.icantalk.service.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import pl.sebox.icantalk.ui.notifications.NotificationsFragment;

@Dao
public interface SentenceDao {
    @Query("SELECT * FROM sentence")
    List<Sentence> getAll();

    @Query("SELECT * FROM sentence WHERE category = 2")
    List<Sentence> getQuickTalkSentences();

    @Query("SELECT * FROM sentence WHERE category = 1")
    List<Sentence> getHighFrequencySentences();

    @Insert
    void insertAll(Sentence... users);

    @Delete
    void delete(Sentence user);
}
