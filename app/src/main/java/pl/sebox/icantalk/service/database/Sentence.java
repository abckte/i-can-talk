package pl.sebox.icantalk.service.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Random;

@Entity
public class Sentence {
//    public Sentence(String sentence, Integer category) {
//        this.uid = (new Random()).nextInt();
//        this.sentence = sentence;
//        this.category = category;
//    }

    public Sentence(int uid, String sentence, Integer category) {
        this.uid = uid;
        this.sentence = sentence;
        this.category = category;
    }

    @PrimaryKey
    public int uid;

    @ColumnInfo(name = "sentence")
    public String sentence;

    @ColumnInfo(name = "category")
    public Integer category;

    @Override
    public String toString() {
        return "Sentence{" +
                "uid=" + uid +
                ", sentence='" + sentence + '\'' +
                ", category=" + category +
                '}';
    }
}
