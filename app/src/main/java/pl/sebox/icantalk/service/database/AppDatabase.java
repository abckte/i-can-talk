package pl.sebox.icantalk.service.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Sentence.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SentenceDao sentenceDao();
}